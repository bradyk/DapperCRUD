﻿namespace CodeGenerator
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.tableList = new System.Windows.Forms.CheckedListBox();
            this.Namespace = new CCWin.SkinControl.SkinTextBox();
            this.SqlServerHost = new CCWin.SkinControl.SkinTextBox();
            this.DatabaseName = new CCWin.SkinControl.SkinTextBox();
            this.Username = new CCWin.SkinControl.SkinTextBox();
            this.skinLabel1 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel2 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel3 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel4 = new CCWin.SkinControl.SkinLabel();
            this.skinLabel5 = new CCWin.SkinControl.SkinLabel();
            this.Password = new CCWin.SkinControl.SkinTextBox();
            this.TestButton = new System.Windows.Forms.Button();
            this.GenerateGoGo = new System.Windows.Forms.Button();
            this.skinLabel6 = new CCWin.SkinControl.SkinLabel();
            this.FileSavePath = new CCWin.SkinControl.SkinTextBox();
            this.SelectFile = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tableList
            // 
            this.tableList.BackColor = System.Drawing.SystemColors.Window;
            this.tableList.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tableList.FormattingEnabled = true;
            this.tableList.Location = new System.Drawing.Point(372, 93);
            this.tableList.Name = "tableList";
            this.tableList.Size = new System.Drawing.Size(240, 310);
            this.tableList.TabIndex = 1;
            // 
            // Namespace
            // 
            this.Namespace.BackColor = System.Drawing.Color.Transparent;
            this.Namespace.DownBack = null;
            this.Namespace.Icon = null;
            this.Namespace.IconIsButton = false;
            this.Namespace.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.Namespace.IsPasswordChat = '\0';
            this.Namespace.IsSystemPasswordChar = false;
            this.Namespace.Lines = new string[0];
            this.Namespace.Location = new System.Drawing.Point(115, 93);
            this.Namespace.Margin = new System.Windows.Forms.Padding(0);
            this.Namespace.MaxLength = 32767;
            this.Namespace.MinimumSize = new System.Drawing.Size(28, 28);
            this.Namespace.MouseBack = null;
            this.Namespace.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.Namespace.Multiline = false;
            this.Namespace.Name = "Namespace";
            this.Namespace.NormlBack = null;
            this.Namespace.Padding = new System.Windows.Forms.Padding(5);
            this.Namespace.ReadOnly = false;
            this.Namespace.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Namespace.Size = new System.Drawing.Size(177, 28);
            // 
            // 
            // 
            this.Namespace.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Namespace.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Namespace.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.Namespace.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.Namespace.SkinTxt.Name = "BaseText";
            this.Namespace.SkinTxt.Size = new System.Drawing.Size(167, 18);
            this.Namespace.SkinTxt.TabIndex = 0;
            this.Namespace.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.Namespace.SkinTxt.WaterText = "";
            this.Namespace.TabIndex = 2;
            this.Namespace.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Namespace.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.Namespace.WaterText = "";
            this.Namespace.WordWrap = true;
            // 
            // SqlServerHost
            // 
            this.SqlServerHost.BackColor = System.Drawing.Color.Transparent;
            this.SqlServerHost.DownBack = null;
            this.SqlServerHost.Icon = null;
            this.SqlServerHost.IconIsButton = false;
            this.SqlServerHost.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.SqlServerHost.IsPasswordChat = '\0';
            this.SqlServerHost.IsSystemPasswordChar = false;
            this.SqlServerHost.Lines = new string[0];
            this.SqlServerHost.Location = new System.Drawing.Point(115, 148);
            this.SqlServerHost.Margin = new System.Windows.Forms.Padding(0);
            this.SqlServerHost.MaxLength = 32767;
            this.SqlServerHost.MinimumSize = new System.Drawing.Size(28, 28);
            this.SqlServerHost.MouseBack = null;
            this.SqlServerHost.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.SqlServerHost.Multiline = false;
            this.SqlServerHost.Name = "SqlServerHost";
            this.SqlServerHost.NormlBack = null;
            this.SqlServerHost.Padding = new System.Windows.Forms.Padding(5);
            this.SqlServerHost.ReadOnly = false;
            this.SqlServerHost.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.SqlServerHost.Size = new System.Drawing.Size(177, 28);
            // 
            // 
            // 
            this.SqlServerHost.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.SqlServerHost.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SqlServerHost.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.SqlServerHost.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.SqlServerHost.SkinTxt.Name = "BaseText";
            this.SqlServerHost.SkinTxt.Size = new System.Drawing.Size(167, 18);
            this.SqlServerHost.SkinTxt.TabIndex = 0;
            this.SqlServerHost.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.SqlServerHost.SkinTxt.WaterText = "";
            this.SqlServerHost.TabIndex = 2;
            this.SqlServerHost.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.SqlServerHost.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.SqlServerHost.WaterText = "";
            this.SqlServerHost.WordWrap = true;
            // 
            // DatabaseName
            // 
            this.DatabaseName.BackColor = System.Drawing.Color.Transparent;
            this.DatabaseName.DownBack = null;
            this.DatabaseName.Icon = null;
            this.DatabaseName.IconIsButton = false;
            this.DatabaseName.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.DatabaseName.IsPasswordChat = '\0';
            this.DatabaseName.IsSystemPasswordChar = false;
            this.DatabaseName.Lines = new string[0];
            this.DatabaseName.Location = new System.Drawing.Point(115, 205);
            this.DatabaseName.Margin = new System.Windows.Forms.Padding(0);
            this.DatabaseName.MaxLength = 32767;
            this.DatabaseName.MinimumSize = new System.Drawing.Size(28, 28);
            this.DatabaseName.MouseBack = null;
            this.DatabaseName.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.DatabaseName.Multiline = false;
            this.DatabaseName.Name = "DatabaseName";
            this.DatabaseName.NormlBack = null;
            this.DatabaseName.Padding = new System.Windows.Forms.Padding(5);
            this.DatabaseName.ReadOnly = false;
            this.DatabaseName.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.DatabaseName.Size = new System.Drawing.Size(177, 28);
            // 
            // 
            // 
            this.DatabaseName.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DatabaseName.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DatabaseName.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.DatabaseName.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.DatabaseName.SkinTxt.Name = "BaseText";
            this.DatabaseName.SkinTxt.Size = new System.Drawing.Size(167, 18);
            this.DatabaseName.SkinTxt.TabIndex = 0;
            this.DatabaseName.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.DatabaseName.SkinTxt.WaterText = "";
            this.DatabaseName.TabIndex = 2;
            this.DatabaseName.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.DatabaseName.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.DatabaseName.WaterText = "";
            this.DatabaseName.WordWrap = true;
            // 
            // Username
            // 
            this.Username.BackColor = System.Drawing.Color.Transparent;
            this.Username.DownBack = null;
            this.Username.Icon = null;
            this.Username.IconIsButton = false;
            this.Username.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.Username.IsPasswordChat = '\0';
            this.Username.IsSystemPasswordChar = false;
            this.Username.Lines = new string[0];
            this.Username.Location = new System.Drawing.Point(115, 260);
            this.Username.Margin = new System.Windows.Forms.Padding(0);
            this.Username.MaxLength = 32767;
            this.Username.MinimumSize = new System.Drawing.Size(28, 28);
            this.Username.MouseBack = null;
            this.Username.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.Username.Multiline = false;
            this.Username.Name = "Username";
            this.Username.NormlBack = null;
            this.Username.Padding = new System.Windows.Forms.Padding(5);
            this.Username.ReadOnly = false;
            this.Username.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Username.Size = new System.Drawing.Size(177, 28);
            // 
            // 
            // 
            this.Username.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Username.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Username.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.Username.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.Username.SkinTxt.Name = "BaseText";
            this.Username.SkinTxt.Size = new System.Drawing.Size(167, 18);
            this.Username.SkinTxt.TabIndex = 0;
            this.Username.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.Username.SkinTxt.WaterText = "";
            this.Username.TabIndex = 2;
            this.Username.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Username.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.Username.WaterText = "";
            this.Username.WordWrap = true;
            // 
            // skinLabel1
            // 
            this.skinLabel1.AutoSize = true;
            this.skinLabel1.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel1.BorderColor = System.Drawing.Color.White;
            this.skinLabel1.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel1.Location = new System.Drawing.Point(83, 76);
            this.skinLabel1.Name = "skinLabel1";
            this.skinLabel1.Size = new System.Drawing.Size(122, 17);
            this.skinLabel1.TabIndex = 3;
            this.skinLabel1.Text = "Default Namespace";
            // 
            // skinLabel2
            // 
            this.skinLabel2.AutoSize = true;
            this.skinLabel2.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel2.BorderColor = System.Drawing.Color.White;
            this.skinLabel2.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel2.Location = new System.Drawing.Point(83, 131);
            this.skinLabel2.Name = "skinLabel2";
            this.skinLabel2.Size = new System.Drawing.Size(92, 17);
            this.skinLabel2.TabIndex = 3;
            this.skinLabel2.Text = "SqlServer host";
            // 
            // skinLabel3
            // 
            this.skinLabel3.AutoSize = true;
            this.skinLabel3.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel3.BorderColor = System.Drawing.Color.White;
            this.skinLabel3.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel3.Location = new System.Drawing.Point(83, 188);
            this.skinLabel3.Name = "skinLabel3";
            this.skinLabel3.Size = new System.Drawing.Size(99, 17);
            this.skinLabel3.TabIndex = 3;
            this.skinLabel3.Text = "Database name";
            // 
            // skinLabel4
            // 
            this.skinLabel4.AutoSize = true;
            this.skinLabel4.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel4.BorderColor = System.Drawing.Color.White;
            this.skinLabel4.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel4.Location = new System.Drawing.Point(83, 243);
            this.skinLabel4.Name = "skinLabel4";
            this.skinLabel4.Size = new System.Drawing.Size(67, 17);
            this.skinLabel4.TabIndex = 3;
            this.skinLabel4.Text = "Username";
            // 
            // skinLabel5
            // 
            this.skinLabel5.AutoSize = true;
            this.skinLabel5.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel5.BorderColor = System.Drawing.Color.White;
            this.skinLabel5.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel5.Location = new System.Drawing.Point(83, 299);
            this.skinLabel5.Name = "skinLabel5";
            this.skinLabel5.Size = new System.Drawing.Size(64, 17);
            this.skinLabel5.TabIndex = 3;
            this.skinLabel5.Text = "Password";
            // 
            // Password
            // 
            this.Password.BackColor = System.Drawing.Color.Transparent;
            this.Password.DownBack = null;
            this.Password.Icon = null;
            this.Password.IconIsButton = false;
            this.Password.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.Password.IsPasswordChat = '\0';
            this.Password.IsSystemPasswordChar = false;
            this.Password.Lines = new string[0];
            this.Password.Location = new System.Drawing.Point(115, 316);
            this.Password.Margin = new System.Windows.Forms.Padding(0);
            this.Password.MaxLength = 32767;
            this.Password.MinimumSize = new System.Drawing.Size(28, 28);
            this.Password.MouseBack = null;
            this.Password.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.Password.Multiline = false;
            this.Password.Name = "Password";
            this.Password.NormlBack = null;
            this.Password.Padding = new System.Windows.Forms.Padding(5);
            this.Password.ReadOnly = false;
            this.Password.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.Password.Size = new System.Drawing.Size(177, 28);
            // 
            // 
            // 
            this.Password.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Password.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Password.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.Password.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.Password.SkinTxt.Name = "BaseText";
            this.Password.SkinTxt.Size = new System.Drawing.Size(167, 18);
            this.Password.SkinTxt.TabIndex = 0;
            this.Password.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.Password.SkinTxt.WaterText = "";
            this.Password.TabIndex = 2;
            this.Password.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.Password.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.Password.WaterText = "";
            this.Password.WordWrap = true;
            // 
            // TestButton
            // 
            this.TestButton.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.TestButton.Location = new System.Drawing.Point(115, 425);
            this.TestButton.Name = "TestButton";
            this.TestButton.Size = new System.Drawing.Size(177, 38);
            this.TestButton.TabIndex = 4;
            this.TestButton.Text = "测试连接";
            this.TestButton.UseVisualStyleBackColor = true;
            this.TestButton.Click += new System.EventHandler(this.TestButton_Click);
            // 
            // GenerateGoGo
            // 
            this.GenerateGoGo.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.GenerateGoGo.Location = new System.Drawing.Point(372, 425);
            this.GenerateGoGo.Name = "GenerateGoGo";
            this.GenerateGoGo.Size = new System.Drawing.Size(240, 38);
            this.GenerateGoGo.TabIndex = 5;
            this.GenerateGoGo.Text = "生成";
            this.GenerateGoGo.UseVisualStyleBackColor = true;
            this.GenerateGoGo.Click += new System.EventHandler(this.GenerateGoGo_Click_1);
            // 
            // skinLabel6
            // 
            this.skinLabel6.AutoSize = true;
            this.skinLabel6.BackColor = System.Drawing.Color.Transparent;
            this.skinLabel6.BorderColor = System.Drawing.Color.White;
            this.skinLabel6.Font = new System.Drawing.Font("微软雅黑", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.skinLabel6.Location = new System.Drawing.Point(83, 356);
            this.skinLabel6.Name = "skinLabel6";
            this.skinLabel6.Size = new System.Drawing.Size(87, 17);
            this.skinLabel6.TabIndex = 3;
            this.skinLabel6.Text = "File save path";
            // 
            // FileSavePath
            // 
            this.FileSavePath.BackColor = System.Drawing.Color.Transparent;
            this.FileSavePath.DownBack = null;
            this.FileSavePath.Icon = null;
            this.FileSavePath.IconIsButton = false;
            this.FileSavePath.IconMouseState = CCWin.SkinClass.ControlState.Normal;
            this.FileSavePath.IsPasswordChat = '\0';
            this.FileSavePath.IsSystemPasswordChar = false;
            this.FileSavePath.Lines = new string[0];
            this.FileSavePath.Location = new System.Drawing.Point(115, 373);
            this.FileSavePath.Margin = new System.Windows.Forms.Padding(0);
            this.FileSavePath.MaxLength = 32767;
            this.FileSavePath.MinimumSize = new System.Drawing.Size(28, 28);
            this.FileSavePath.MouseBack = null;
            this.FileSavePath.MouseState = CCWin.SkinClass.ControlState.Normal;
            this.FileSavePath.Multiline = false;
            this.FileSavePath.Name = "FileSavePath";
            this.FileSavePath.NormlBack = null;
            this.FileSavePath.Padding = new System.Windows.Forms.Padding(5);
            this.FileSavePath.ReadOnly = false;
            this.FileSavePath.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.FileSavePath.Size = new System.Drawing.Size(121, 28);
            // 
            // 
            // 
            this.FileSavePath.SkinTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.FileSavePath.SkinTxt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FileSavePath.SkinTxt.Font = new System.Drawing.Font("微软雅黑", 9.75F);
            this.FileSavePath.SkinTxt.Location = new System.Drawing.Point(5, 5);
            this.FileSavePath.SkinTxt.Name = "BaseText";
            this.FileSavePath.SkinTxt.Size = new System.Drawing.Size(111, 18);
            this.FileSavePath.SkinTxt.TabIndex = 0;
            this.FileSavePath.SkinTxt.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.FileSavePath.SkinTxt.WaterText = "";
            this.FileSavePath.TabIndex = 2;
            this.FileSavePath.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.FileSavePath.WaterColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(127)))), ((int)(((byte)(127)))));
            this.FileSavePath.WaterText = "";
            this.FileSavePath.WordWrap = true;
            // 
            // SelectFile
            // 
            this.SelectFile.Location = new System.Drawing.Point(240, 373);
            this.SelectFile.Name = "SelectFile";
            this.SelectFile.Size = new System.Drawing.Size(52, 28);
            this.SelectFile.TabIndex = 6;
            this.SelectFile.Text = "选择";
            this.SelectFile.UseVisualStyleBackColor = true;
            this.SelectFile.Click += new System.EventHandler(this.SelectFile_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(615, 93);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(38, 25);
            this.button1.TabIndex = 7;
            this.button1.Text = "刷新";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.ClientSize = new System.Drawing.Size(696, 519);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.SelectFile);
            this.Controls.Add(this.GenerateGoGo);
            this.Controls.Add(this.TestButton);
            this.Controls.Add(this.skinLabel6);
            this.Controls.Add(this.skinLabel5);
            this.Controls.Add(this.skinLabel4);
            this.Controls.Add(this.skinLabel3);
            this.Controls.Add(this.skinLabel2);
            this.Controls.Add(this.skinLabel1);
            this.Controls.Add(this.FileSavePath);
            this.Controls.Add(this.Password);
            this.Controls.Add(this.Username);
            this.Controls.Add(this.DatabaseName);
            this.Controls.Add(this.SqlServerHost);
            this.Controls.Add(this.Namespace);
            this.Controls.Add(this.tableList);
            this.Name = "MainForm";
            this.Text = "生成器";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.CheckedListBox tableList;
        private CCWin.SkinControl.SkinTextBox Namespace;
        private CCWin.SkinControl.SkinTextBox SqlServerHost;
        private CCWin.SkinControl.SkinTextBox DatabaseName;
        private CCWin.SkinControl.SkinTextBox Username;
        private CCWin.SkinControl.SkinLabel skinLabel1;
        private CCWin.SkinControl.SkinLabel skinLabel2;
        private CCWin.SkinControl.SkinLabel skinLabel3;
        private CCWin.SkinControl.SkinLabel skinLabel4;
        private CCWin.SkinControl.SkinLabel skinLabel5;
        private CCWin.SkinControl.SkinTextBox Password;
        private System.Windows.Forms.Button TestButton;
        private System.Windows.Forms.Button GenerateGoGo;
        private CCWin.SkinControl.SkinLabel skinLabel6;
        private CCWin.SkinControl.SkinTextBox FileSavePath;
        private System.Windows.Forms.Button SelectFile;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button button1;
    }
}

