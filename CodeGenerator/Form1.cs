﻿using CCWin;
using CCWin.SkinControl;
using CodeGenerator.Core;
using CodeGenerator.Dal.DbHelper.Base;
using NVelocity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeGenerator
{
    public partial class MainForm : Skin_Mac
    {
        private string NamespaceStr = "";
        private string SqlServerHostStr = "";
        private string DatabaseNameStr = "";
        private string UsernameStr = "";
        private string PasswordStr = "";
        private string FileSavePathStr = "";

        public MainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 生成代码
        /// </summary>
        private void GenerateGoGo_Click_1(object sender, EventArgs e)
        {
            DBModel dbObj = new DBModel()
            {
                host = SqlServerHostStr,
                dbName = DatabaseNameStr,
                username = UsernameStr,
                password = PasswordStr
            };

            //获取所有表结构信息
            List<TableModel> tableInfoList = new DbTable().GetTableInfoList(dbObj);

            if (this.tableList.CheckedItems.Count > 0)
            {
                foreach (string item in this.tableList.CheckedItems)
                {
                    List<TableModel> objList = new List<TableModel>();
                    objList = tableInfoList.Where(r => r.tableName == item).ToList();

                    //生成实体类
                    new NVelocityHelper().GeneratedModel(NamespaceStr, item, objList, FileSavePathStr);
                    //生成方法类
                    new NVelocityHelper().GeneratedMethod(NamespaceStr, item, objList, FileSavePathStr, dbObj);
                }

                MessageBox.Show("生成成功！", "ok");
            }

        }

        /// <summary>
        /// 选择文件夹
        /// </summary>
        private void SelectFile_Click(object sender, EventArgs e)
        {
            if (this.folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                if (this.folderBrowserDialog1.SelectedPath.Trim() != "")
                    this.FileSavePath.Text = this.folderBrowserDialog1.SelectedPath.Trim();
            }
        }

        /// <summary>
        /// 测试连接
        /// </summary>
        private void TestButton_Click(object sender, EventArgs e)
        {
            NamespaceStr = this.Namespace.Text;
            SqlServerHostStr = this.SqlServerHost.Text;
            DatabaseNameStr = this.DatabaseName.Text;
            UsernameStr = this.Username.Text;
            PasswordStr = this.Password.Text;
            FileSavePathStr = this.FileSavePath.Text;

            if (NamespaceStr != "" && SqlServerHostStr != "" && DatabaseNameStr != "" && UsernameStr != "" && PasswordStr != "" && FileSavePathStr != "")
            {
                DBModel dbObj = new DBModel()
                {
                    host = SqlServerHostStr,
                    dbName = DatabaseNameStr,
                    username = UsernameStr,
                    password = PasswordStr
                };

                if (new DbHelper().DBTestConnection(dbObj))
                {
                    LoadTable();
                }
                else
                {
                    MessageBox.Show("数据库连接失败！", "error");
                }
            }
            else
            {
                MessageBox.Show("请将表单填写完整！", "error");
            }


        }

        /// <summary>
        /// 加载数据库表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (NamespaceStr != "" && SqlServerHostStr != "" && DatabaseNameStr != "" && UsernameStr != "" && PasswordStr != "" && FileSavePathStr != "")
            {
                tableList.Items.Clear();

                DBModel dbObj = new DBModel()
                {
                    host = SqlServerHostStr,
                    dbName = DatabaseNameStr,
                    username = UsernameStr,
                    password = PasswordStr
                };

                if (new DbHelper().DBTestConnection(dbObj))
                {
                    LoadTable();
                }
                else
                {
                    MessageBox.Show("数据库连接失败！", "error");
                }
            }
            else
            {
                MessageBox.Show("请将表单填写完整！", "error");
            }

           
        }

        /// <summary>
        /// 加载数据库表
        /// </summary>
        private void LoadTable()
        {
            DBModel dbObj = new DBModel()
            {
                host = SqlServerHostStr,
                dbName = DatabaseNameStr,
                username = UsernameStr,
                password = PasswordStr
            };

            List<string> list = new DbTable().GetTableList(dbObj);

            foreach (string tab in list)
            {
                tableList.Items.Add(tab);
            }
        }
    }
}
