﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeGenerator.Dal.DbHelper.Base
{
    public class DBModel
    {
        public string host { get; set; }
        public string dbName { get; set; }
        public string username { get; set; }
        public string password { get; set; }
    }
}
