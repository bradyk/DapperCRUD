﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeGenerator.Dal.DbHelper.Base
{
    public class TableModel
    {
        /// <summary>
        /// 是否为主键
        /// </summary>
        public string isPk { get; set; }
        /// <summary>
        /// 表名称
        /// </summary>
        public string tableName { get; set; }
        /// <summary>
        /// 列名称
        /// </summary>
        public string columnName { get; set; }
        /// <summary>
        /// 属性说明
        /// </summary>
        public string explain { get; set; }
        /// <summary>
        /// 字段类型
        /// </summary>
        public string dataType { get; set; }
        /// <summary>
        /// 属性长度
        /// </summary>
        public string dataLen { get; set; }
        /// <summary>
        /// 小数位数
        /// </summary>
        public string digit { get; set; }
        /// <summary>
        /// 是否为空
        /// </summary>
        public string isEmpty { get; set; }
        /// <summary>
        /// 默认值
        /// </summary>
        public string defaultValue { get; set; }

    }
}
