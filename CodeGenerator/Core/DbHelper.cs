﻿using CodeGenerator.Dal.DbHelper.Base;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeGenerator.Core
{
    public class DbHelper
    {
        /// <summary>
        /// 测试数据库连接
        /// </summary>
        public bool DBTestConnection(DBModel dbObj)
        {

            SqlConnection mySqlConnection; 
            string ConnectionString = "";
            bool IsCanConnectioned = false;
            ConnectionString =string.Format("server={0};user id={1};pwd={2};database={3}", dbObj.host, dbObj.username, dbObj.password, dbObj.dbName);
            mySqlConnection = new SqlConnection(ConnectionString);
            try
            {
                mySqlConnection.Open();
                IsCanConnectioned = true;
            }
            catch
            {
                IsCanConnectioned = false;
            }
            finally
            {
                mySqlConnection.Close();
            }
            if (mySqlConnection.State == ConnectionState.Closed || mySqlConnection.State == ConnectionState.Broken)
            {
                return IsCanConnectioned;
            }
            else
            {
                return IsCanConnectioned;
            }
        }
    }
}

