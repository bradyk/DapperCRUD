﻿using CodeGenerator.Dal.DbHelper.Base;
using NVelocity;
using NVelocity.App;
using NVelocity.Runtime;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeGenerator.Core
{
    public class NVelocityHelper
    {
        /// <summary>
        /// 生成代码主方法
        /// </summary>
        /// <param name="vltContext"></param>
        /// <param name="tpName"></param>
        /// <param name="savePath"></param>
        public void GeneratedFile(VelocityContext vltContext, string tpName, string savePath)
        {
            VelocityEngine vltEngine = new VelocityEngine();
            // 文件型模板，还可以是 assembly ，则使用资源文件
            vltEngine.SetProperty(RuntimeConstants.RESOURCE_LOADER, "file");
            // 模板存放目录
            vltEngine.SetProperty(RuntimeConstants.FILE_RESOURCE_LOADER_PATH, AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "\\Template");
            //初始化
            vltEngine.Init();

            using (StringWriter strWriter = new StringWriter())
            {
                //模板与数据合并
                vltEngine.MergeTemplate(tpName, "utf-8", vltContext, strWriter);
                //写入合并后的数据
                StreamWriter strmsave = new StreamWriter(savePath, false, System.Text.Encoding.Default);
                strmsave.Write(strWriter.ToString());
                strmsave.Close();

            }

        }

        /// <summary>
        /// 生成实体类
        /// </summary>
        public void GeneratedModel(string namespaceName, string tableName, List<TableModel> objList, string savePath)
        {
            if (!Directory.Exists(savePath + "\\Model"))
            {
                Directory.CreateDirectory(savePath + "\\Model");
            }

            VelocityContext vltContext = new VelocityContext();
            vltContext.Put("namespaceName", namespaceName + ".Entities");
            vltContext.Put("tableName", tableName);
            vltContext.Put("tableInfo", objList);
            new NVelocityHelper().GeneratedFile(vltContext, "ModeTp.txt", savePath + "\\Model\\" + tableName + "EntityObjects.cs");

        }

        /// <summary>
        /// 生成方法类
        /// </summary>
        public void GeneratedMethod(string namespaceName, string tableName, List<TableModel> objList, string savePath, DBModel dbObj)
        {
            if (!Directory.Exists(savePath + "\\Dap"))
            {
                Directory.CreateDirectory(savePath + "\\Dap");
            }

            string pkName = "";
            string SqlInsert = "";
            string SqlInsertValues = "";
            string SqlUpdate = "";

            foreach (var obj in objList)
            {
                if ("√".Equals(obj.isPk))
                {
                    pkName = obj.columnName;

                    if (!new DbTable().GetPKisSelfGrowth(dbObj, pkName))
                    {
                        if (!"int".Equals(obj.dataType))
                        {
                            SqlInsert += obj.columnName + " ,";
                            SqlInsertValues += "@" + obj.columnName + " ,";

                            SqlUpdate += obj.columnName + "= @" + obj.columnName + " ,";
                        }
                    }

                }
                else
                {
                    SqlInsert += obj.columnName + " ,";
                    SqlInsertValues += "@" + obj.columnName + " ,";

                    SqlUpdate += obj.columnName + "= @" + obj.columnName + " ,";
                }
            }

            if (objList.Count > 0)
            {
                VelocityContext vltContext = new VelocityContext();
                vltContext.Put("namespaceName", namespaceName + ".Entities");
                vltContext.Put("className", tableName + "Dap");
                vltContext.Put("tableName", tableName);
                vltContext.Put("pkName", pkName);
                vltContext.Put("SqlInsert", SqlInsert.TrimEnd(','));
                vltContext.Put("SqlInsertValues", SqlInsertValues.TrimEnd(','));
                vltContext.Put("SqlUpdate", SqlUpdate.TrimEnd(','));
                new NVelocityHelper().GeneratedFile(vltContext, "MethodTp.txt", savePath + "\\Dap\\" + tableName + "DapperAccessPrincipal.cs");
            }

        }
    }
}
